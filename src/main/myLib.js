function calculate (a, b) {
  return a + b;
}

function calculateAndPrint (a, b) {
  document.getElementById('p2').innerHTML = calculate(a,b);
}

function calculateAndSend (a, b, callBack) {
       var xmlHttp = new XMLHttpRequest();
       xmlHttp.onreadystatechange = function() {
           if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
               console.log(xmlHttp.responseText);
               document.getElementById('p3b').appendChild(document.createTextNode(xmlHttp.responseText));
               callBack(null, xmlHttp.responseText);
               } else if (xmlHttp.status == 404) {
                   callBack(new Error("Page not found"), xmlHttp.responseText);
               }
       }
       var value = a + b;
       xmlHttp.open("GET", 'index.html?param1=' + value, true); // these parameter is ignored currently
       xmlHttp.send(null);
}

function printCode () {
    document.write(document.currentScript.parentNode.getAttribute('onclick'));
}

QUnit.test( "calculate will sum up A and B", function( assert ) {
  assert.ok( calculate(2,2) == 4, "Passed!" );
});


QUnit.test( "calculate will send AJAX", function( assert ) {
  var done = assert.async();
  calculateAndSend (2,2, function (error, data) {
     if (error) throw Error;
     assert.ok (error === null, "No errors");
     done();
  } );
});